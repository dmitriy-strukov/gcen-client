require 'spec_helper'
require 'gcen_client/api'

RSpec.describe GcenClient::API do
  let(:instance) { GcenClient::API.instance }
  let(:env) { double }

  before do
    class Rails
    end

    allow(Rails).to receive(:env).and_return(env)
    allow(env).to receive(:test?).and_return(true)
  end

  describe '#gcs_balance' do
    let(:client_id) { 102 }
    let(:currency)  { 'GBP' }

    let(:response) do
      {
        status: 401,
        body:   ''
      }
    end

    subject { instance.gcs_balance(client_id: client_id, currency: currency) }

    it { expect(subject).to eq response }
  end
end
