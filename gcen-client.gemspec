
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'gcen_client/version'

Gem::Specification.new do |spec|
  spec.name          = 'gcen_client'
  spec.version       = GcenClient::VERSION
  spec.authors       = ['Dmitriy Strukov']
  spec.email         = ['dmitiry_strukov2011@mail.ru']

  spec.summary       = 'GCEN API Client'
  spec.description   = 'GCEN API: Generic Investment API 0.1.1'
  spec.homepage      = 'http://github.com/evil-raccoon/gcen-client'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end

  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_dependency 'faraday_middleware', '~> 0.10.0'
end
