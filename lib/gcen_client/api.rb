require 'faraday'
require 'json'

require 'singleton'

module GcenClient
  class API
    include Singleton

    API_HOST = 'https://gcen.i-dash.co.uk/Api/'

    attr_reader :gcen_token

    # Authentication
    #
    # With each request, You must send an extra HTTP header, 'Authentication'
    # We suggest using JSON as the data transfer type, hence you should set the Content Type to application/json
    # All API requests must be made over HTTPS in POST format.
    def initialize
      @gcen_token = ENV['GCEN_TOKEN']
    end

    # AML Registration
    # This method will register a client with GCEN
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/AMLRegister
    def aml_register(params:)
      post_request('AMLRegister', params)
    end

    # GCS Balance
    # This method will return the GCS balance for a given client's currency
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/GCSBalance
    def gcs_balance(client_id:, currency:)
      post_request('GCSBalance', {
        ClientId: client_id,
        Currency: currency
      })
    end

    # Bank transfer deposit
    # Instructs GCEN of impending bank transfer deposit. Supplies the bank details and reference necessary.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/InvestmentDeposit
    def investment_deposit(params:)
      post_request('InvestmentDeposit', params)
    end

    # Card Payment Deposit
    # Make an investment/deposit by credit/debit card.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/CardPayment
    def card_payment(params:)
      post_request('CardPayment', params)
    end

    # Check Card Payment Response
    # This method checks whether a card payment was authorised and payment was successfully taken.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/CheckCardPaymentResponse
    def check_card_payment_response(ref:, return_mac:)
      post_request('CheckCardPaymentResponse', {
        REF: ref,
        RETURNMAC: return_mac
      })
    end

    # Payment return from balance
    # Instructs GCEN to make a payment out using one of the clients' GCS balances.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/PaymentReturnFromGCSBalance
    def payment_return_from_gcs_balance(params:)
      post_request('PaymentReturnFromGCSBalance', params)
    end

    # GCS Balance Transfer
    # Instructs GCEN to move money from one GCS client balance to another. Internally, GCEN
    # creates a virutal deal for the "FromClient". Money is moved from the "FromClient" balance to
    # the virtual deal, and money is moved from the virutal deal to the "ToClient" balance.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/GCSBalanceTransfer
    def gcs_balance_transfer(params:)
      post_request('GCSBalanceTransfer', params)
    end

    # FX Quote
    # Generates a unique quote from GCEN to exchange currency. Returns an FX Online Deal
    # Quotation with a quotation reference. Quote is valid for purchase up to 30 minutes.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/GetFXQuote
    def get_fx_quote(params:)
      post_request('GetFXQuote', params)
    end

    # Confirm FX Quote - Card Payment
    # This method confirms a given FX quote and sets up the payment system so the user can
    # place their transaction.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/ConfirmQuoteWithCardPayment
    def confirm_quote_with_card_payment(params:)
      post_request('ConfirmQuoteWithCardPayment', params)
    end

    # Confirm FX Quote - Bank Transfer
    # This method confirms a given FX quote and returns the amount and bank account that the
    # client must pay in to.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/ConfirmQuoteWithBankTransfer
    def confirm_quote_with_bank_transfer(params:)
      post_request('ConfirmQuoteWithBankTransfer', params)
    end

    # Credit GCS Balance
    # Makes a manual adjustment to a client's GCS balance.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/CreditGCSBalance
    def credit_gcs_balance(client_id:, currency:, amount:)
      post_request('CreditGCSBalance', {
        ClientId: client_id,
        Currency: currency,
        Amount:   amount
      })
    end

    def check_cdd(params:)
      post_request('CheckCDD', params)
    end

    # Upload Documentation
    # This method allows you to send GCEN the client's Id documents for them to verify and
    # confirm the identity of the client.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/UploadIdDocuments
    def upload_id_documents(client_id:, document_1:, document_2:)
      post_request('UploadIdDocuments', {
        ClientId:  client_id,
        Document1: document_1,
        Document2: document_2
      })
    end

    # Get List of Client deals
    # Returns a list of a clients deals.
    #
    # @since 0.1.0
    #
    # @see https://app.gcen.co.uk/Api/GetDealHistory/<ClientId>
    def get_deal_history(params:, gcen_id:)
      post_request("GetDealHistory/#{gcen_id}", params)
    end

    private

      def post_request(endpoint_name, body, content_type='application/json')
        http = Faraday.new(url: API_HOST) do |param|
          param.headers['Authentication'] = gcen_token
          param.adapter Faraday.default_adapter
        end

        response = http.post do |request|
          request.url endpoint_name

          request.body = body.to_json
          request.headers['Content-Type'] = content_type
        end

        if response.body.empty?
          {
            status: response.status,
            body:   response.body
          }
        else
          JSON.parse(response.body)
        end
      end
  end
end
