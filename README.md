# Gcen::Client

The `gcen-client` library provides access to the Gcen API from applications in the Ruby language. It
includes a pre-defined set of methods.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gcen-client'
```
